# TDD

V- Cloner ce repo
V- Rechercher et expliquer avec vos mots les principes du tdd, et l'ajouter a ce Readme.
    Les ttd servent à tester des fonctions indépendament du reste du programme, de plus ils servent à construire la fonction au fur et à mesure des tests.

EC- Observer fizzbuzz.test.js puis completer fizzbuzz.js pour qu'il passe les tests ("npm run test" pour lancer les tests)

- Creer un fichier calc.js dans /src contenant une fonction calc()
- Creer un fichier calc.test.js dans /test
- Ecrire les test d'une fonction calc qui permet d'additionner/soustraire/multiplier/diviser 2 nombres envoyé a la fonction calc, 
avec gestion de toutes les erreurs possibles, en utilisant la méthode tdd.
- commit entre chaque ecriture de test et chaque ecriture de fonction
