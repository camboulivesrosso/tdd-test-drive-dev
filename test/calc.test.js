const expect = require('chai').expect;
const {add} = require('../src/calc');

describe("Addition", () => {
    it("Should return '45' for 15 + 30 ", () => {
        expect(add(15, 30)).to.equal(45);
    });
    it("Should return 'ERREUR' if params 1 is absent", () => {
        expect(add(9)).to.equal('ERREUR !');
    });
    it("Should return 'ERREUR' if params 1 is not a number", () => {
        expect(add('a', 48)).to.equal('ERREUR !');
    });
    it("Should return 'ERREUR' if params 2 is absent", () => {
        expect(add(1006, 'grrr')).to.equal('ERREUR !');
    });
    it("Should return '4,7' for 1,4 + 3,3 ", () => {
        expect(add(1.4, 3.3)).to.equal(4.7);
    });
    it("Should return 'ERREUR' if one params is too big with Max safe Integer", () => {
        expect(add(15, Number.MAX_SAFE_INTEGER)).to.equal('ERREUR !');
    });
    it("Should return 'ERREUR' if one params is too big with an integer", () => {
        expect(add(140, 10000000000000000000000000000000000)).to.equal('ERREUR !');
    });
    it("Should return 'ERREUR' if one params is an boolean", () => {
        expect(add(86, true)).to.equal('ERREUR !');
    });
    it("Should return '0.000000000000000000000002' for 0.000000000000000000000001 + 0.000000000000000000000001", () => {
        expect(add(0.000000000000000000000001, 0.000000000000000000000001)).to.equal(0.000000000000000000000002);
    });
    

})