const fizzbuzz = (num) => {
    if( num  % 5 == 0 && num % 7 == 0){
        return 'fizzbuzz';
    }
    if(num == undefined){
        return 'Error!';
    }
    return (num % 5 == 0 ? "buzz" : (num % 7 == 0?"fizz":""));
}

exports.fizzbuzz = fizzbuzz;